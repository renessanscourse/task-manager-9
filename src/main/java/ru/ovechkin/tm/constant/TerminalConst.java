package ru.ovechkin.tm.constant;

public interface TerminalConst {

    String ARG_HELP = "-h";

    String ARG_INFO = "-i";

    String ARG_VERSION = "-v";

    String ARG_ABOUT = "-a";

    String ARG_ARGUMENTS = "-ar";

    String ARG_COMMANDS = "-c";

    String CMD_HELP = "help";

    String CMD_VERSION = "version";

    String CMD_ABOUT = "about";

    String CMD_EXIT = "exit";

    String CMD_INFO = "info";

    String CMD_ARGUMENTS = "arguments";

    String CMD_COMMANDS = "commands";

}
