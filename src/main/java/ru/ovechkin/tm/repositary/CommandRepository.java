package ru.ovechkin.tm.repositary;

import ru.ovechkin.tm.api.ICommandRepository;
import ru.ovechkin.tm.constant.TerminalConst;
import ru.ovechkin.tm.model.TerminalCommand;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final TerminalCommand HELP = new TerminalCommand(
            TerminalConst.CMD_HELP, TerminalConst.ARG_HELP, " Display terminal commands"
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            TerminalConst.CMD_ABOUT, TerminalConst.ARG_ABOUT, " Show developer info"
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            TerminalConst.CMD_VERSION, TerminalConst.ARG_VERSION, " Show version info"
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            TerminalConst.CMD_INFO, TerminalConst.ARG_INFO, " Display system's info"
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            TerminalConst.CMD_EXIT, null, " Close application"
    );

    public static final TerminalCommand HELP_COMMANDS = new TerminalCommand(
            TerminalConst.CMD_COMMANDS, TerminalConst.ARG_COMMANDS, " Show available commands"
    );

    public static final TerminalCommand HELP_ARGUMENTS = new TerminalCommand(
            TerminalConst.CMD_ARGUMENTS, TerminalConst.ARG_ARGUMENTS, " Show available arguments"
    );

    private final TerminalCommand[] TERMINAL_COMMANDS = new TerminalCommand[]{
            HELP, ABOUT, VERSION, INFO, EXIT, HELP_COMMANDS, HELP_ARGUMENTS
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public TerminalCommand[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }

}
