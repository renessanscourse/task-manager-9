package ru.ovechkin.tm;

import ru.ovechkin.tm.constant.TerminalConst;
import ru.ovechkin.tm.model.TerminalCommand;
import ru.ovechkin.tm.repositary.CommandRepository;
import ru.ovechkin.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static final CommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        displayWelcome();
        if (parseArgs(args)) System.exit(0);
        process();
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            System.out.print("Enter command: ");
            command = scanner.nextLine();
            parseCommand(command);
            parseArg(command);
            System.out.println();
        }
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        parseCommand(arg);
        return true;
    }

    private static void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.ARG_VERSION:
                showVersion();
                break;
            case TerminalConst.ARG_ABOUT:
                showAbout();
                break;
            case TerminalConst.ARG_HELP:
                showHelp();
                break;
            case TerminalConst.ARG_INFO:
                showInfo();
                break;
            case TerminalConst.ARG_ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.ARG_COMMANDS:
                showCommands();
                break;
        }
    }

    private static void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case TerminalConst.CMD_HELP:
                showHelp();
                break;
            case TerminalConst.CMD_INFO:
                showInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                showCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
        }
    }

    private static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory (bytes): " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM (bytes): " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM (bytes): " + usedMemoryFormat);
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayWelcome() {
        System.out.println();
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.9");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME:\tOvechkin Roman");
        System.out.println("E-MAIL:\troman@ovechkin.ru");
    }

    private static void showCommands() {
        final String[] commands = COMMAND_REPOSITORY.getCommands();
        for (final String command : commands) System.out.println(command);
    }

    private static void showArguments() {
        final String[] arguments = COMMAND_REPOSITORY.getArgs();
        for (final String argument : arguments) System.out.println(argument);
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final TerminalCommand[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final TerminalCommand command : commands) System.out.println(command);
    }

}
